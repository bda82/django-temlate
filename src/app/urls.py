import imp
from django.urls import path
from django.contrib.auth import views as auth_views

from app.views.index import index
from app.views.auth import register


app_name = 'app'


urlpatterns = [
    path('', index, name='index'),
    path('auth/login/', auth_views.LoginView.as_view(template_name='auth/login.html'), name='login'),
    path('auth/logout/', auth_views.LogoutView.as_view(next_page='app:index'), name='logout'),
    path('auth/register/', register, name='register'),
]
