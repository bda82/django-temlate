from django.shortcuts import render, redirect
from django.contrib import messages

from app.forms.auth import UserRegisterForm


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.form)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Welcome {username}!')
            return redirect('app:index')
    else:
        form = UserRegisterForm()
        return render(request, 'auth/register.html', {'form': form})
